#!/usr/bin/bash env

if [ -$# -lt 2]; then
    echo 'Usage joplin-import <notebook> <md-files>'
fi

notebook=$1
md_files=$2 #TODO splice array

#TODO ask confirmation, check if notebook exists already,
# if it does ask user or choose defaults or read from config file
joplin mkbook "${notebook}"
for i in ${md_files[@]}; do 
    joplin import -- "${i}" "${notebook}";
done
